#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <threads.h>
#include <assert.h>
#include <pthread.h>

long long int ptreadlocalstorage_temps = 0;
long long int threadlocal_temps = 0;
long long int tss_temps = 0;
long long int systid_temps = 0;
long long int idself_temps = 0;
long long int idpthself_temps = 0;
long long int grain_temps =0;

#define NBMESURE 10000
#define NBTH 1

#define gettid() syscall(SYS_gettid);

typedef struct expe {
    void (*f)();
    long long int *n;
} Expe;

thread_local int locid=123;
thread_local tss_t clef;


int boucle(void *arg) {
    Expe *e = arg;
    locid = 122;
    tss_create(&clef, NULL);
    int idinstack = 123;
    tss_set(clef, &idinstack);
    for(int i=0; i < NBMESURE; i++) {
	struct timespec debut={}, fin={};
	clock_gettime(CLOCK_MONOTONIC_RAW, &debut);
	e->f();
	clock_gettime(CLOCK_MONOTONIC_RAW, &fin);
	*(e->n) += (fin.tv_sec - debut.tv_sec) * 1000000000LL +
	    (fin.tv_nsec - debut.tv_nsec);
    }
}

void gettid_from_linux(void) {
    int mytid= gettid();
}

void granularite(void) {
}

void id_self(void) {
    int myid = thrd_current();
}
void idpth_self(void) {
    int myid = pthread_self();
}

void id_thloc(void) {
    assert(locid == 122);
}


void id_dans_tss(void) {
    int *idp = tss_get(clef);
    assert(*idp == 123);
}

int main() {
    thrd_t th[NBTH];

    for(int k=0; k< 5; k++) {

	Expe e = { granularite, &grain_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d fonction vide, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );

	e = (Expe){ gettid_from_linux, &systid_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d syscall, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );

	e = (Expe){ id_self, &idself_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d thrd_current C11, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );

	e = (Expe){ idpth_self, &idpthself_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d pthread_self, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );

	e = (Expe){ id_thloc, &threadlocal_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d thread_local, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );
	// vérifie qu'il n'a pas changé
	assert(locid == 123);

	e = (Expe){ id_dans_tss, &tss_temps };
	for(int i=0; i < NBTH; i++) {
	    thrd_create(&th[i], boucle, &e);
	}
	for(int i=0; i < NBTH; i++) {
	    thrd_join(th[i], NULL);
	}
	printf("%d tss, en moyenne : %lld ns\n", NBMESURE, (*(e.n))/NBMESURE );
	// vérifie qu'il n'a pas changé
	assert(locid == 123);
	// reset des compteurs de temps
	ptreadlocalstorage_temps = 0;
	threadlocal_temps = 0;
	tss_temps = 0;
	systid_temps = 0;
	idself_temps = 0;
	idpthself_temps = 0;
	grain_temps =0;
	printf("\n");
    }
}
