* Concurrency
** Contents: 360 minutes (6 hours)
   - The problem of concurrency :: memory versus CPU: 30
   - Basics of Monitor in C :: Tony Hoare ideas: barrier, allocator,
        reader-writer with full variables, philosophe with 3 states to
        make signal optional: 90
   - Basics of Semaphore in C :: Dijkstra, why does it lost (3 states
        philosophe example) and why is it still useful (prod-cons with
        parallel access usage): 60
   - Post Box in MPI? :: Printers example: 30
   - Channels in Go :: Parallel DU in Go. Simulate a
                       semaphore. Rendez-Vous as in Ada. Difficulties
                       for avoiding dead-lock: fibo example : 30
   - Implementation: From Dekker to Futex: 120
 
