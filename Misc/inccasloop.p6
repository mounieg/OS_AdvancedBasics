#!/usr/bin/perl6

use v6;

my int $i = 0;
my @threads = do for 1..5 -> $id {
    Thread.start: {
	for ^10000 {
	    cas $i, -> $new {
		$i + 1
	    }
	}
    }
}
.join for @threads;
say "$i sur 50000";
