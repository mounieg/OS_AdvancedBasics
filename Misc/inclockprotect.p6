#!/usr/bin/perl6

use v6;

my int $i = 0;
my $lock = Lock.new;
my @threads = do for 1..5 -> $id {
    Thread.start: {
	$lock.protect: {
	    $i++ for ^10000;
	}
    }
}
.join for @threads;
say "$i sur 50000";
