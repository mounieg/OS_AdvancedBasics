#!/usr/bin/perl6
use v6;

my @threads = do for 1..5 -> $id {
    Thread.start: {
	say "Hi from $id";
	sleep 1;
	say "Bye from $id"
    }
}
.join for @threads;
