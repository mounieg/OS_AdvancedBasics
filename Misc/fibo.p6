use v6;

sub fibo(Int $n, $chan) {

    if $n <= 2 {
	$chan.send($n);
	return;
    }


    my $res1 = Channel.new;
    my $res2 = Channel.new;

    my $pro = start { fibo $n-1, $res1; }
    fibo $n-2, $res2;

    await $pro;
    my $res = $res1.receive + $res2.receive;
    $res1.close;
    $res2.close;
    $chan.send($res);
}

my $res = Channel.new;
fibo(11, $res);
say $res.receive;
$res.close;
