#!/usr/bin/perl6

use v6;

my @pro;
for 1..20 -> $i {
    push @pro, start {
	say "Hi from task $i";
	sleep 1;
	say "task $i done"
    };
};
await @pro;
