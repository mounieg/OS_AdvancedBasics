#!/usr/bin/perl6

use v6;

for 1..20 -> $i {
    $*SCHEDULER.cue: {
	say "Hi from task $i";
	sleep 1;
	say "task $i done"
    }
}

sleep;
