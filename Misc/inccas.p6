#!/usr/bin/perl6

use v6;

my int $i = 0;
my @threads = do for 1..5 -> $id {
    Thread.start: {
	for ^10000 {
	    loop {
		my $old = $i + 0;
		my $new = $old + 1;
		last if cas($i, $old, $new) === $old;
	    }
	}
    }
}
.join for @threads;
say "$i sur 50000";
