#!/usr/bin/perl6

use v6;

my int $i = 0;
my $lock = Lock.new;
my @threads = do for 1..5 -> $id {
    Thread.start: {
	$lock.lock();
	$i++ for ^10000;
	$lock.unlock();
    }
}
.join for @threads;
say "$i sur 50000";
